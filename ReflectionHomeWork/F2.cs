using System;

namespace ReflectionHomeWork;

public class F2
{
    private readonly double _d1;
    public string StringProp { get; set; }
    private byte ByteProp { get; set; }

    public int i1;
    protected float f1;
    private long l1;
    public static Guid guid;

    public F2()
    {
        _d1 = Random.Shared.NextDouble();
    }


    public static F2 Get()
    {
        guid = Guid.NewGuid();
        return new F2()
        {
            f1 = 0.5f,
            i1 = 1,
            StringProp = "test",
            l1 = long.MaxValue,
            ByteProp = byte.MaxValue,
        };
    }
}