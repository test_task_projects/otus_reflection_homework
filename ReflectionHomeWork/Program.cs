﻿// See https://aka.ms/new-console-template for more information

using System.Diagnostics;
using Newtonsoft.Json;
using ReflectionHomeWork;

const int IterationCount = 10000;

JsonSerializerSettings settings = new JsonSerializerSettings()
{
    Formatting = Formatting.None
};

TestSerialization();
TestDeserializeCsv();
//TestSerializationWithConsole();
TestJsonSerialization();
TestJsonDeserialization();


void TestSerialization()
{
    var stopWatch = new Stopwatch();
    stopWatch.Start();
    for (int i = 0; i < IterationCount; i++)
    {
        MyCSVHelper.Serialize(F.Get());
    }
    stopWatch.Stop();
    Console.WriteLine($"MyCSV Serialization Total ms:{stopWatch.ElapsedMilliseconds}");
}

void TestSerializationWithConsole()
{
    var stopWatch = new Stopwatch();
    stopWatch.Start();
    for (int i = 0; i < IterationCount; i++)
    {
        Console.WriteLine(MyCSVHelper.Serialize(F.Get()));
    }
    stopWatch.Stop();
    Console.WriteLine($"MyCSV Serialization ToConsole Total ms:{stopWatch.ElapsedMilliseconds}");
}

void TestDeserializeCsv()
{
    var csv = MyCSVHelper.Serialize(F.Get());
    var stopWatch = new Stopwatch();
    stopWatch.Start();
    for (int i = 0; i < IterationCount; i++)
    {
        MyCSVHelper.Deserialize<F>(csv);
    }
    stopWatch.Stop();
    Console.WriteLine($"MyCSV Deserialization Total ms:{stopWatch.ElapsedMilliseconds}");
}

void TestJsonSerialization()
{
    var stopWatch = new Stopwatch();
    stopWatch.Start();
    for (int i = 0; i < IterationCount; i++)
    {
        JsonConvert.SerializeObject(F.Get(), settings);
    }
    stopWatch.Stop();
    Console.WriteLine($"Newtonsoft Json Serialization Total ms:{stopWatch.ElapsedMilliseconds}");
}

void TestJsonDeserialization()
{
    var json = JsonConvert.SerializeObject(F.Get(), settings);
    var stopWatch = new Stopwatch();
    stopWatch.Start();
    for (int i = 0; i < IterationCount; i++)
    {
        JsonConvert.DeserializeObject<F>(json);
    }
    stopWatch.Stop();
    Console.WriteLine($"Newtonsoft Json Deserialization Total ms:{stopWatch.ElapsedMilliseconds}");
}