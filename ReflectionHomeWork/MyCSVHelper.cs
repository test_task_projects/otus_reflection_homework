using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using CsvHelper;

namespace ReflectionHomeWork;

public static class MyCSVHelper
{
    private const BindingFlags Bindings =
        BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static;

    private static string? GetValue(string valueType, object? value)
    {
        if (value is null) return string.Empty;
        if (valueType == nameof(Single) || valueType == nameof(Double) || valueType == nameof(Decimal))
        {
            if (double.TryParse(value.ToString(), out double result))
            {
                return result.ToString(CultureInfo.InvariantCulture);
            }
        }


        return value.ToString();
    }

    public static string Serialize<T>(T data) where T : class
    {
        var type = data.GetType();
        var fields = type.GetFields(Bindings);

        var sb = new StringBuilder();

        sb.AppendLine("Name,Value");

        foreach (var fieldInfo in fields)
        {
            if (fieldInfo.Name.Contains($"k__BackingField")) continue;

            sb.AppendLine($"{fieldInfo.Name},{GetValue(fieldInfo.FieldType.Name, fieldInfo.GetValue(data))}");
        }

        var properties = type.GetProperties(Bindings);
        foreach (var propertyInfo in properties)
        {
            sb.AppendLine(
                $"{propertyInfo.Name},{GetValue(propertyInfo.PropertyType.Name, propertyInfo.GetValue(data))}");
        }

        return sb.ToString();
    }

    public static T Deserialize<T>(string csv) where T : class
    {
        try
        {
            var instance = Activator.CreateInstance<T>();
            var fields = instance.GetType().GetFields(Bindings);
            var properties = instance.GetType().GetProperties(Bindings);

            using var stringReader = new StringReader(csv);

            while (true)
            {
                var line = stringReader.ReadLine();
                if (line == null)
                {
                    break;
                }

                var fieldRow = line.Split(",");

                var targetField = fields.FirstOrDefault(info => info.Name == fieldRow[0]);
                var property = properties.FirstOrDefault(info => info.Name == fieldRow[0]);

                if (targetField != null)
                {
                    var valueString = GetValue(fieldRow[1]);
                    var converter = TypeDescriptor.GetConverter(targetField.FieldType);
                    targetField.SetValue(instance, converter.ConvertFromString(valueString));
                }

                if (property != null)
                {
                    var valueString = GetValue(fieldRow[1]);
                    var converter = TypeDescriptor.GetConverter(property.PropertyType);
                    property.SetValue(instance, converter.ConvertFromString(valueString));
                }
            }

            return instance;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        return default;
    }

    private static string GetValue(string valueStr)
    {
        if (!double.TryParse(valueStr, out double val))
        {
            return valueStr.Replace(".", ",");
        }

        return valueStr;
    }

    private class DoubleConverterEx : DoubleConverter
    {
        public override object? ConvertFrom(ITypeDescriptorContext? context, CultureInfo? culture, object value)
        {
            if (value is string text)
            {
                text = text.Trim();
                return Double.Parse(text, NumberStyles.Float, CultureInfo.CurrentCulture);
            }

            return base.ConvertFrom(context, culture, value);
        }
    }
}